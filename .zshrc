
export MAKEFLAGS="-j8"
export PATH="$HOME/.emacs.d/bin:$PATH"

alias ls='ls --color=auto'
alias la='ls -A'
alias updatedb='sudo updatedb'
alias vpn='sudo protonvpn'
alias ..='cd ..'

autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

bindkey  "^[[3~"  delete-char

PROMPT="%c %# "

pfetch
