syntax on

filetype off

set nocompatible
set nu
set noswapfile
set nobackup
set noundofile
set clipboard=unnamedplus
set incsearch
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set smartcase
set laststatus=2
set t_Co=256
set smarttab
set noshowmode
set fileformat=unix
set encoding=utf-8

"noremap h <left>
"noremap t <down>
"noremap n <up>
"noremap s <right>

set rtp+=~/.vim
set rtp+=~/.vim/autoload/plug.vim

call plug#begin('~/.vim/plugged')

Plug 'dracula/vim',{'as':'dracula'}
Plug 'itchyny/lightline.vim' 
Plug 'lilydjwg/colorizer'

call plug#end()

let g:lightline = {'colorscheme': 'dracula'}
let g:rehash256 = 1

set background=dark

"  highlight LineNr           ctermfg=8    ctermbg=none    cterm=none
"  highlight CursorLineNr     ctermfg=7    ctermbg=8       cterm=none
"  highlight VertSplit        ctermfg=0    ctermbg=8       cterm=none
"  highlight Statement        ctermfg=2    ctermbg=none    cterm=none
"  highlight Directory        ctermfg=4    ctermbg=none    cterm=none
"  highlight StatusLine       ctermfg=7    ctermbg=8       cterm=none
"  highlight StatusLineNC     ctermfg=7    ctermbg=8       cterm=none
"  highlight Comment          ctermfg=4    ctermbg=none    cterm=none
"  highlight Constant         ctermfg=12   ctermbg=none    cterm=none
"  highlight Special          ctermfg=4    ctermbg=none    cterm=none
"  highlight Identifier       ctermfg=6    ctermbg=none    cterm=none
"  highlight PreProc          ctermfg=5    ctermbg=none    cterm=none
"  highlight String           ctermfg=12   ctermbg=none    cterm=none
"  highlight Number           ctermfg=1    ctermbg=none    cterm=none
"  highlight Function         ctermfg=1    ctermbg=none    cterm=none
