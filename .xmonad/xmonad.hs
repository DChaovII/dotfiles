--------------------------------------------------------------------------
-- IMPORTS
--------------------------------------------------------------------------

import XMonad
import Data.Monoid
import System.Exit

import XMonad.Actions.CycleWS
import XMonad.Actions.NoBorders
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.Spacing
import XMonad.Prompt
import XMonad.Prompt.Shell
import XMonad.Util.Cursor
import XMonad.Util.Run
import XMonad.Util.SpawnOnce

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

--------------------------------------------------------------------------
-- DEFINE SOME VARIABLES
--------------------------------------------------------------------------

-- Define Default Terminal Emulator
myTerminal :: [Char]
myTerminal = "alacritty"

-- Hover to Focus
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Click to Focus
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Define Border Width
myBorderWidth :: Dimension
myBorderWidth = 2

-- Define Modkey as Super Key
myModMask :: KeyMask
myModMask = mod4Mask

-- Make a "Clickable" Workspaces and Set Its Names 
xmobarEscape :: String -> String
xmobarEscape = concatMap doubleLts
  where
        doubleLts '<' = "<<"
        doubleLts x   = [x]

myClickableWorkspaces :: [String]
myClickableWorkspaces = clickable . map xmobarEscape
               $ ["web", "dev", "sys", "game", "mus", "etc"]
  where
        clickable l = [ "<action=xdotool key super+" ++ show n ++ ">" ++ ws ++ "</action>" |
                      (i,ws) <- zip [1 .. 9] l,
                      let n = i ]

-- Set Border Color
myNormalBorderColor :: [Char]
myFocusedBorderColor :: [Char]
myNormalBorderColor = "#282A36"
myFocusedBorderColor = "#FF79C6"

-- Set Default Browser
myBrowser :: [Char]
myBrowser = "qutebrowser"

-- Set Default Editor
myEditor :: [Char]
-- myEditor = "emacsclient -c -a 'emacs' "
myEditor = "alacritty -e vim "

--------------------------------------------------------------------------
-- KEY BINDINGS
--------------------------------------------------------------------------

myKeys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
myKeys conf@XConfig {XMonad.modMask = modm} = M.fromList $

    -- launch a terminal
    [ ((modm,               xK_Return), spawn $ XMonad.terminal conf)

    -- launch run launcher
    , ((modm,               xK_l     ), shellPrompt myXPConfig)

    -- shift focus to next screen
    , ((modm,               xK_v     ), nextScreen )

    -- shift focus to previous screen
    , ((modm,               xK_w     ), prevScreen )

    -- launch file manager
    , ((modm,              xK_period ), spawn "pcmanfm" )
    
    -- support media keys 
    , ((0,                0x1008ff13 ), spawn "pamixer -i 5")
    , ((0,                0x1008ff12 ), spawn "pamixer -t")
    , ((0,                0x1008ff11 ), spawn "pamixer -d 5")
 
    -- screenshot with flameshot
    , ((modm .|. shiftMask, xK_o     ), spawn "flameshot gui -p /home/dchaovii/Pictures")
    , ((0,                  xK_Print ), spawn "flameshot screen -p /home/dchaovii/Pictures")

    -- toggle borders on/off
    , ((modm,               xK_x     ), withFocused toggleBorder)

    -- open qutebrowser
    , ((modm .|. shiftMask, xK_comma ), spawn myBrowser)
   
    -- open emacs
    -- , ((modm .|. shiftMask, xK_period), spawn myEditor)

    -- open this config
    -- , ((modm .|. shiftMask, xK_j     ), spawn (myEditor ++ "~/.xmonad/xmonad.hs"))

    -- open xmonad errors
    -- , ((modm .|. shiftMask, xK_q     ), spawn (myEditor ++ "~/.xmonad/xmonad.errors"))

    -- close focused window
    , ((modm,               0x27     ), kill)

    -- Change to the next keyboard layout
    , ((modm,               xK_space ), spawn "layoutChanger")

    --  Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modm,               xK_b     ), refresh)

    -- Move focus to the next window
    , ((modm,               xK_Tab   ), windows W.focusDown)

    -- Move focus to the next window
    , ((modm,               xK_h     ), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm,               xK_t     ), windows W.focusUp  )

    -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_h     ), windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask, xK_t     ), windows W.swapUp    )

    -- Shrink the master area
    , ((modm,               xK_d     ), sendMessage Shrink)

    -- Expand the master area
    , ((modm,               xK_n     ), sendMessage Expand)

    -- Push window back into tiling
    , ((modm,               xK_y     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    -- , ((modm              , xK_w     ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    -- , ((modm              , xK_v     ), sendMessage (IncMasterN (-1)))

    -- Quit xmonad
    , ((modm .|. shiftMask, 0x27     ), io exitSuccess)

    -- Restart xmonad
    , ((modm .|. shiftMask, xK_p     ), spawn "xmonad --recompile; xmonad --restart")

    ]

    ++

    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]

--------------------------------------------------------------------------
-- MOUSE BINDINGS
--------------------------------------------------------------------------

myMouseBindings :: XConfig l -> M.Map (KeyMask, Button) (Window -> X ())
myMouseBindings XConfig {XMonad.modMask = modm} = M.fromList

    -- Set window floating mode
    [ ((modm, button1), \w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster)

    -- Bring window forward
    , ((modm, button2), \w -> focus w >> windows W.shiftMaster)
    
    -- Resize window
    , ((modm, button3), \w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster)
    ]

------------------------------------------------------------------------
-- LAYOUTS 
------------------------------------------------------------------------
mySpacing :: l a
            -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing = spacingRaw False (Border 6 6 6 6) True (Border 6 6 6 6) True

myLayout :: ModifiedLayout
            AvoidStruts (ModifiedLayout Spacing Tall) Window
myLayout = avoidStruts $ mySpacing tiled
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio 

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

------------------------------------------------------------------------
-- XMONAD PROMPT
------------------------------------------------------------------------

myXPConfig :: XPConfig
myXPConfig = def
      { font                = "xft:Mononoki Nerd Font:pixelsize=14"
      , bgColor             = "#282A36"
      , fgColor             = "#F8F8F2" --"#DEDEDE"
      , bgHLight            = "#F8F8F2" -- "#DEDEDE"
      , fgHLight            = "#282A36"
      , promptBorderWidth   = 0
      , position            = Top
      , height              = 20
      , historySize         = 256
      , historyFilter       = id
      , defaultText         = []
      , autoComplete        = Nothing
      , showCompletionOnTab = False
      , alwaysHighlight     = True
      , maxComplRows        = Just 3
      }

------------------------------------------------------------------------
-- EXTRA STUFF
------------------------------------------------------------------------

myManageHook :: Query (Endo WindowSet)
myManageHook = composeAll
    [ className =? "mpv"                      --> doFloat
    , className =? "File-roller"              --> doFloat
    , title =? "Removable medium is inserted" --> doFloat
    , title =? "Power Manager"                --> doFloat
    , title =? "Transmission"                 --> doFloat
    , title =? "Chat"                         --> doFloat
    ]

myEventHook :: Event -> X All
myEventHook = mempty

myLogHook :: X ()
myLogHook = return ()

myStartupHook :: X ()
myStartupHook = do 
        setDefaultCursor xC_left_ptr
        spawnOnce "nitrogen --restore &"
        spawnOnce "picom &"
        spawnOnce "xfce4-power-manager &"
        spawnOnce "lxsession &"
        spawnOnce "flameshot &"
        spawnOnce "trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 --tint 0x282A36 --height 20 &"
        spawnOnce "dunst &"
        spawnOnce "steam -silent &"

main :: IO ()
main = do
  xmproc0 <- spawnPipe "xmobar -x 0 /home/dchaovii/.config/xmobar/xmobarrc0"
  xmproc1 <- spawnPipe "xmobar -x 1 /home/dchaovii/.config/xmobar/xmobarrc1"
  xmonad $ docks defaults
      { manageHook = manageDocks <+> (isFullscreen --> doFullFloat) <+> myManageHook
      , handleEventHook = fullscreenEventHook
      , layoutHook = myLayout
      , workspaces = myClickableWorkspaces
      , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = \x -> hPutStrLn xmproc0 x >> hPutStrLn xmproc1 x
                        , ppCurrent = xmobarColor "#F8F8F2" "" . wrap "[" "]"
                        , ppVisible = xmobarColor "#F8F8F2" ""
                        , ppHiddenNoWindows = xmobarColor "#9E9E9C" ""
                        , ppHidden = xmobarColor "#CBCBC7" "" . wrap "*" ""
                        , ppTitle   = xmobarColor "#F8F8F2"  "" . shorten 60
                        , ppSep = " <fc=#F8F8F2>|</fc> "
                        , ppUrgent  = xmobarColor "#FF5555" "" . wrap "!" "!"
                        , ppOrder  = \(ws:l:t:_) -> ws:[t]
                        }
      , modMask = mod4Mask
      }

defaults :: XConfig
                (ModifiedLayout AvoidStruts (ModifiedLayout Spacing Tall))
defaults = def {
      -- Simple Stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myClickableWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- Key Bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- Hooks, Layouts
        layoutHook         = myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        logHook            = myLogHook,
        startupHook        = myStartupHook
    }

