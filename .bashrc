#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export MAKEFLAGS="-j6"

alias ls='ls --color=auto --group-directories-first'
alias la='ls -A'
alias updatedb='doas updatedb'
alias pvpn='protonvpn-cli'
alias ..='cd ..'
alias conf='vim ~/.xmonad/xmonad.hs'
alias err='vim ~/.xmonad/xmonad.errors'
alias clear="clear && pfetch"
alias grep="grep --color=auto"

PS1='\W \$ '

pfetch
